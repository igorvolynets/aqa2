package mypackage;

public interface Animal {

    void growth();
    void eat();
    void weigh();
    void motion();
}
