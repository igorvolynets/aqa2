package mypackage;

public class BeforeAfterCycle {


    static void goWithCondition(int a){
        System.out.println("while");
        while(a>0) {
            System.out.println("Число равно " + a);
            a--;
        }
    }

    static void goWithoutCondition(int b){
        System.out.println("do   while");
        do{
            System.out.println(+b+" введенное число");
            b++;
        }while (b<9);
    }

    public static void main(String[] args) {
        goWithCondition(5);
        goWithoutCondition(9);
    }

}
