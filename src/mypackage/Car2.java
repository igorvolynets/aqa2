package mypackage;

public class Car2 implements Transport, InterfaceTest{
    @Override
    public void go() {
        System.out.println("I'm a car. I'm driving!");

    }

    @Override
    public void stop() {
        System.out.println("I'm a car. We are driving slower");

    }

    @Override
    public void method1() {
        System.out.println("test1");
    }

    @Override
    public void method2() {
        System.out.println("test2");

    }
}
