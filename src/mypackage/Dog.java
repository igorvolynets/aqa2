package mypackage;

public class Dog implements Animal {
    double meters;

    Dog() {
        this.meters=meters;
    }
    Dog(double meters) {
        this.meters=meters;
    }

    @Override
    public void growth() {
        System.out.println("Не знаю своего роста (=");

    }

    public void growth(double meters) {
        System.out.println("Мой рост " + meters+ " метра(ов)");

    }

    public void growth(int foots) {
        System.out.println("Мой рост " + foots+ " фута(ов)");

    }

    @Override
    public void eat() {
        System.out.println("Я ем мяско");
    }

    @Override
    public void weigh() {
        System.out.println("Я не знаю своего веса");
    }

    @Override
    public void motion() {
        System.out.println("Я бегаю");
    }

    public void motion(int distance) {
        System.out.println("Я пробегаю за день " + distance+ " км");
    }

    public static void main(String[] args) {

        Dog dog=new Dog();
        dog.growth(2);
        dog.growth(1.85);
        dog.eat();
        dog.motion();
        dog.motion(15);
        dog.weigh();


    }
}