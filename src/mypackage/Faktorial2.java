package mypackage;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Faktorial2 {

    public static void main(String[] args)throws  Exception {
        System.out.println("Введите число");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int result =1;

        for (int i = 1; i<=n; i++ ) {

            result = result*i;


            System.out.println(result);
        }


    }

}
