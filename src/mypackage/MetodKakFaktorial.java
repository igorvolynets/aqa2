package mypackage;

public class MetodKakFaktorial {

    static double Factorial(double n) {
        double result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Factorial(50));
    }
}
