package mypackage;

public class ProgramTransport {

    public static void main(String[] args) {

        Car2 car = new Car2();
        Plane plane = new Plane();

        car.go();
        car.stop();
        car.method1();
        car.method2();

        plane.go();
        plane.stop();
        plane.setSpeed(500);


    }



}
