package mypackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tovar {

    public static void main(String[] args) throws IOException {


        System.out.println("Чтобы узнать цену за товар введите код от 1 до 3");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int KodTovara = Integer.parseInt(reader.readLine());
        if (KodTovara == 1){
            System.out.println("Кефир стоит 10 рублей");
        }
        else if (KodTovara == 2){
            System.out.println("Батон стоит 20 рублей");
        }
        else if (KodTovara == 3){
            System.out.println("Сало стоит 30 рублей");
        }
        else System.out.println("Неправильно введен код товара");



    }
}
