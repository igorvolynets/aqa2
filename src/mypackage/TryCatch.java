package mypackage;

public class TryCatch {
    public static void main(String[] args) {
        try {
            devide(10, 0);
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } finally {
            System.out.println();

        }


    }

    public static void devide(int a, int b) {
        if (b == 0){
            throw new ArithmeticException("Cannot devide by 0");
        } else {
            System.out.println("Result is: " + a / b);
        }
    }
}