package mypackage;

public class TwoWaysMethod implements Runnable {
    private int balance;

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            increment();
            System.out.println(balance);
        }
    }

    public synchronized void increment() {
        int i = balance;
        balance = i + 1;
    }

    public static void main(String[] args) {
        TwoWaysMethod job = new TwoWaysMethod();
        Thread a = new Thread(job);
        Thread b = new Thread(job);
        a.start();
        b.start();
    }
}